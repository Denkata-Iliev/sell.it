﻿namespace Sell_it.Web.Models
{
    public class ProductCreateViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public Guid CategoryId { get; set; }

        public ICollection<string> ImageUrls { get; set; }

        public ProductCreateViewModel()
        {
            ImageUrls = new List<string>();
        }
    }
}
