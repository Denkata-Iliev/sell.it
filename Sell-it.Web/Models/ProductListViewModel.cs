﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Sell_it.Data.Entities;

namespace Sell_it.Web.Models
{
    public class ProductListViewModel
    {
        public string SearchName { get; set; }
        
        public IList<Product> Products { get; set; }

        public Guid CategoryId { get; set; }

        public string PriceOrder { get; set; }
    }
}
