﻿namespace Sell_it.Web.Models
{
    public class UserRolesViewModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Role { get; set; }
    }
}
