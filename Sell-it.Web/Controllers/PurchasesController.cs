﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.CategoryServices;
using Sell_it.Service.PhotoServices;
using Sell_it.Service.ProductServices;
using Sell_it.Service.PurchaseServices;
using Sell_it.Service.UserServices;

namespace Sell_it.Web.Controllers
{
    [Authorize]
    public class PurchasesController : Controller
    {
        private readonly SellItContext context;
        private readonly UserManager<User> userManager;

        private readonly IPurchaseService purchaseService;
        private readonly IProductService productService;
        private readonly ICategoryService categoryService;
        private readonly IPhotoService photoService;
        private readonly IUserService userService;

        public PurchasesController(SellItContext context,
                                   IPurchaseService purchaseService,
                                   IProductService productService,
                                   ICategoryService categoryService,
                                   IPhotoService photoService,
                                   IUserService userService,
                                   UserManager<User> userManager)
        {
            this.context = context;
            this.purchaseService = purchaseService;
            this.productService = productService;
            this.categoryService = categoryService;
            this.photoService = photoService;
            this.userService = userService;
            this.userManager = userManager;
        }

        [HttpGet]
        public IActionResult Buy(Guid productId)
        {
            Product product = productService.Get(productId);
            string userId = userManager.GetUserId(User);

            if (product == null || userId == product.UserId)
            {
                return RedirectToAction("Index", "Products");
            }

            Purchase purchase = new Purchase();

            product.Seller = userService.Get(product.UserId);
            product.Category = categoryService.GetProductCategory(product.CategoryId);
            product.Photos = photoService.GetProductPhotos(productId);

            purchase.Buyer = userService.Get(userId);
            purchase.Product = product;

            return View(purchase);
        }

        [HttpPost]
        public IActionResult Buy(Purchase purchase)
        {
            Product product = productService.Get(purchase.ProductId);

            product.Seller = userService.Get(product.UserId);
            product.Category = categoryService.GetProductCategory(product.CategoryId);

            purchase.Product = product;
            purchase.Buyer = userService.Get(userManager.GetUserId(User));

            purchaseService.Create(purchase);

            return RedirectToAction(nameof(MyPurchases));
        }

        [HttpGet]
        public IActionResult MyPurchases()
        {
            List<Purchase> purchases = purchaseService.GetAll()
                            .Where(p => p.BuyerId.Equals(userManager.GetUserId(User)))
                            .ToList();

            purchases.ForEach(p => {
                p.Product.Seller = userService.Get(p.Product.UserId);
                p.Product.Category = categoryService.Get(p.Product.CategoryId);
                p.Product.Photos = photoService.GetProductPhotos(p.ProductId);
                });

            return View(purchases);
        }

        [HttpGet]
        public IActionResult Remove(Guid id)
        {
            Purchase purchase = purchaseService.Get(id);
            purchaseService.Delete(purchase);
            return RedirectToAction(nameof(MyPurchases));
        }
    }
}
