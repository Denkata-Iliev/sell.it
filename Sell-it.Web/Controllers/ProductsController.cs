﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.CategoryServices;
using Sell_it.Service.PhotoServices;
using Sell_it.Service.ProductServices;
using Sell_it.Web.Models;

namespace Sell_it.Web.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private readonly SellItContext context;
        private readonly UserManager<User> userManager;

        private readonly IProductService productService;
        private readonly IPhotoService photoService;
        private readonly ICategoryService categoryService;

        public ProductsController(SellItContext context, 
                                  IProductService productService,
                                  IPhotoService photoService,
                                  ICategoryService categoryService,
                                  UserManager<User> userManager)
        {
            this.context = context;
            this.productService = productService;
            this.photoService = photoService;
            this.categoryService = categoryService;
            this.userManager = userManager;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Index()
        {
            ViewData["CategoryId"] = new SelectList(categoryService.GetAll(), "Id", "Name");
            ProductListViewModel model = new ProductListViewModel();
            model.Products = productService.GetAll();
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Index(ProductListViewModel model)
        {
            ProductListViewModel filteredViewModel = FilterViewModel(model);

            ViewData["CategoryId"] = new SelectList(categoryService.GetAll(), "Id", "Name");
            return View(filteredViewModel);
        }

        [HttpGet]
        public IActionResult MyProducts()
        {
            string userId = userManager.GetUserId(User);

            ProductListViewModel model = new ProductListViewModel();
            model.Products = productService.GetAll().Where(p => p.Seller.Id.Equals(userId)).ToList();

            ViewData["CategoryId"] = new SelectList(categoryService.GetAll(), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public IActionResult MyProducts(ProductListViewModel model)
        {
            string userId = userManager.GetUserId(User);

            ProductListViewModel filteredViewModel = FilterViewModel(model);
            filteredViewModel.Products = filteredViewModel.Products.Where(p => p.Seller.Id.Equals(userId)).ToList();
            
            ViewData["CategoryId"] = new SelectList(categoryService.GetAll(), "Id", "Name");
            return View(filteredViewModel);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Details(Guid id)
        {
            Product product = productService.Get(id);
            if (product == null)
            {
                return RedirectToAction(nameof(Index));
            }

            product.Photos = photoService.GetProductPhotos(product.Id);
            product.Category = categoryService.GetProductCategory(product.CategoryId);

            return View(product);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewData["CategoryId"] = new SelectList(categoryService.GetAll(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public IActionResult Create(IFormCollection collection, ProductCreateViewModel model)
        {
            Product product = new Product();

            product.Name = model.Name;
            product.Description = model.Description;

            product.Price = GetParsedProductPrice(collection);

            product.UserId = userManager.GetUserId(User);

            var categoryId = collection["CategoryId"];
            product.Category = categoryService.GetProductCategory(Guid.Parse(categoryId));

            foreach (string imgUrl in model.ImageUrls)
            {
                Photo productPhoto = new Photo()
                {
                    Id = new Guid(),
                    ProductId = product.Id,
                    Path = imgUrl
                };

                product.Photos.Add(productPhoto);
            }

            productService.Create(product);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            Product product = productService.Get(id);
            
            if (product == null || product.UserId != userManager.GetUserId(User))
            {
                return RedirectToAction(nameof(Index));
            }

            ViewData["CategoryId"] = new SelectList(categoryService.GetAll(), "Id", "Name");

            ProductCreateViewModel model = new ProductCreateViewModel
            {
                Id = id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                CategoryId = product.CategoryId
            };

            product.Photos = photoService.GetProductPhotos(product.Id);
            
            foreach(Photo photo in product.Photos)
            {
                model.ImageUrls.Add(photo.Path);
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(IFormCollection collection, ProductCreateViewModel model)
        {
            Product product = new Product
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description
            };

            product.Photos = photoService.GetProductPhotos(product.Id);

            photoService.UpdateProductPhotos(product, model.ImageUrls);

            product.UserId = userManager.GetUserId(User);

            var categoryId = collection["CategoryId"];
            product.Category = categoryService.GetProductCategory(Guid.Parse(categoryId));

            product.Price = GetParsedProductPrice(collection);

            productService.Update(product);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            Product product = productService.Get(id);

            if (product == null || product.UserId != userManager.GetUserId(User))
            {
                return RedirectToAction(nameof(Index));
            }

            product.Photos = photoService.GetProductPhotos(product.Id);
            product.Category = categoryService.GetProductCategory(product.CategoryId);

            return View(product);
        }

        [HttpPost]
        public IActionResult Delete(Product product)
        {
            productService.Delete(product);
            return RedirectToAction(nameof(Index));
        }

        private static decimal GetParsedProductPrice(IFormCollection collection)
        {
            string stringPrice = collection["Price"].ToString().Replace('.', ',');
            decimal productPrice = decimal.Parse(stringPrice);
            return productPrice;
        }

        private ProductListViewModel FilterViewModel(ProductListViewModel model)
        {
            List<Product> filtered = new List<Product>();

            switch (model.PriceOrder)
            {
                case "Ascending":
                    filtered = productService.Filter(model.CategoryId, model.SearchName);
                    break;
                case "Descending":
                    filtered = productService.FilterDesc(model.CategoryId, model.SearchName);
                    break;
            }

            ProductListViewModel filteredViewModel = new ProductListViewModel()
            {
                Products = filtered,
                CategoryId = model.CategoryId
            };
            return filteredViewModel;
        }
    }
}
