﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Web.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Sell_it.Service.UserServices;

namespace Sell_it.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RolesController : Controller
    {
        private readonly SellItContext context;
        private readonly UserManager<User> userManager;

        private readonly IUserService userService;

        public RolesController(SellItContext context,
                               IUserService userService,
                               UserManager<User> userManager)
        {
            this.context = context;
            this.userService = userService;
            this.userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<UserRolesViewModel> userRoles = new List<UserRolesViewModel>();

            userRoles = context.Users.Select(u => new UserRolesViewModel
            {
                UserId = u.Id,
                UserName = u.UserName,
                Role = userManager.GetRolesAsync(u).Result.ToList()[0]
            }).ToList();

            return View(userRoles);
        }

        [HttpGet]
        public IActionResult Manage(string userId)
        {
            User user = userService.Get(userId);

            if (user == null)
            {
                return RedirectToAction(nameof(Index));
            }

            UserRolesViewModel model = new UserRolesViewModel
            {
                UserId = user.Id,
                UserName = user.UserName,
                Role = userManager.GetRolesAsync(user).Result.ToList()[0]
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Manage(UserRolesViewModel model)
        {
            User user = userService.Get(model.UserId);

            if (user == null)
            {
                RedirectToAction(nameof(Index));
            }

            var userRoles = await userManager.GetRolesAsync(user);
            await userManager.RemoveFromRolesAsync(user, userRoles);

            await userManager.AddToRoleAsync(user, model.Role);

            return RedirectToAction(nameof(Index));
        }
    }
}
