﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.CategoryServices;

namespace Sell_it.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoriesController : Controller
    {
        private const string EXISTING_CATEGORY_ERROR_MESSAGE = "A Category with that Name already exists!";

        private readonly SellItContext context;
        private readonly ICategoryService categoryService;

        public CategoriesController(SellItContext context,
                                    ICategoryService categoryService)
        {
            this.context = context;
            this.categoryService = categoryService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(categoryService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Category category)
        {
            bool doesCategoryExist = categoryService.GetAll()
                                .Where(c => c.Name.ToLower().Equals(category.Name.ToLower()))
                                .Any();

            if (doesCategoryExist)
            {
                ViewBag.ErrorMessage = EXISTING_CATEGORY_ERROR_MESSAGE;
                return View();
            }

            categoryService.Create(category);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            Category category = categoryService.Get(id);

            if (category == null)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(category);
        }

        [HttpPost]
        public IActionResult Edit(Category category)
        {
            bool doesCategoryExist = categoryService.GetAll()
                                .Where(c => c.Name.ToLower().Equals(category.Name.ToLower()))
                                .Any();

            if (doesCategoryExist)
            {
                ViewBag.ErrorMessage = EXISTING_CATEGORY_ERROR_MESSAGE;
                return View(category);
            }

            categoryService.Update(category);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            Category category = categoryService.Get(id);

            if (category == null)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(category);
        }

        [HttpPost]
        public IActionResult Delete(Category category)
        {
            categoryService.Delete(category);
            return RedirectToAction(nameof(Index));
        }
    }
}
