﻿const inputNumber = parseInt($("#img-number").val());

const existingImgUrls = [];
for (let i = 0; i < 5; i++) {
    if ($(`#img-url-${i + 1}`).val()) {
        existingImgUrls.push($(`#img-url-${i + 1}`).val().toString());
    } else {
        existingImgUrls.push("");
    }
}

generateInputs(inputNumber);
$("#img-number").change(() => generateInputs(parseInt($("#img-number").val())));

function generateInputs(numberOfInputs) {
    removeInputs();
    for (let i = 0; i < numberOfInputs; i++) {
        const imgUrlInputDiv = $(`<div class="form-group mt-2 img-urls">
                                    <label for="ImageUrls[${i}]" class="control-label">Image Url ${i + 1}</label>
                                    <input id="img-url-${i + 1}" name="ImageUrls[${i}]" class="form-control" value="${existingImgUrls[i]}"/>
                                    <span id="error-msg-${i + 1}" class="text-danger"></span>
                                </div>
                              `);
        $("input[type=submit]").before(imgUrlInputDiv);
    }
}

function removeInputs() {
    $(".img-urls").remove();
}

$("#Form").submit((event) => validateImageUrls(parseInt($("#img-number").val())));

function validateImageUrls(numberOfInputs) {
    for (let i = 1; i <= numberOfInputs; i++) {
        if (!$(`#img-url-${i}`).val()) {
            event.preventDefault();
            $(`#error-msg-${i}`).text(`The Image Url ${i} field is required.`);
        }
    }
}