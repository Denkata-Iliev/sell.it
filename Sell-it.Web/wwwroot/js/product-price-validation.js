﻿const priceInput = document.getElementById("Price");
const createForm = document.getElementById("Form");
const priceValidationSpan = document.getElementById("PriceValidation");

createForm.addEventListener("submit", (event) => validateForm(event));

function validateForm(event) {
    const priceValue = parseFloat(priceInput.value);

    if (!priceInput.value) {
        event.preventDefault();
        priceValidationSpan.innerHTML = "The Price field is required."
    }

    if (priceValue <= 0) {
        event.preventDefault();
        priceValidationSpan.innerHTML = "Price must be higher than 0.";
    }
}