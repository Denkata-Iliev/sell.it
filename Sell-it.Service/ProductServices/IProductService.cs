﻿using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.ProductServices
{
    public interface IProductService : IBaseService<Product>
    {
        public List<Product> Filter(Guid categoryId, string searchName);

        public List<Product> FilterDesc(Guid categoryId, string searchName);
    }
}
