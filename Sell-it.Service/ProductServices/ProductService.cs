﻿using Microsoft.EntityFrameworkCore;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.ProductServices
{
    public class ProductService : BaseService<Product>, IProductService
    {
        private readonly SellItContext context;
        private readonly DbSet<Product> dbSet;
        public ProductService(SellItContext context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<Product>();
        }

        public new List<Product> GetAll()
        {
            return dbSet
                .Select(p => new Product()
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Category = new Category()
                    {
                        Id = p.Category.Id,
                        Name = p.Category.Name,
                    },
                    Price = p.Price,
                    Seller = new User()
                    {
                        Id = p.UserId,
                        UserName = p.Seller.UserName
                    },
                    Photos = context.Photos.Where(photo => photo.ProductId.Equals(p.Id)).ToList()
                }).ToList();
        }

        public List<Product> Filter(Guid categoryId, string searchName)
        {
            bool isSearchNameNullOrEmpty = string.IsNullOrEmpty(searchName);
            bool isCategoryIdEmpty = categoryId.Equals(Guid.Empty);

            if (isCategoryIdEmpty && isSearchNameNullOrEmpty)
            {
                return FilterByPrice();
            }

            if (!isCategoryIdEmpty && isSearchNameNullOrEmpty)
            {
                return FilterByCategory(categoryId);
            }

            if (isCategoryIdEmpty && !isSearchNameNullOrEmpty)
            {
                return FilterByName(searchName);
            }

            return FilterByCategoryAndName(categoryId, searchName);
        }

        private List<Product> FilterByCategoryAndName(Guid categoryId, string searchName)
        {
            return FilterByPrice().Where(p => p.Category.Id.Equals(categoryId) && p.Name.ToLower().Contains(searchName.ToLower())).ToList();
        }

        private List<Product> FilterByName(string searchName)
        {
            return FilterByPrice().Where(p => p.Name.ToLower().Contains(searchName.ToLower())).ToList();
        }

        private List<Product> FilterByCategory(Guid categoryId)
        {
            return FilterByPrice().Where(p => p.Category.Id.Equals(categoryId)).ToList();
        }

        private List<Product> FilterByPrice()
        {
            return GetAll().OrderBy(p => p.Price).ToList();
        }

        public List<Product> FilterDesc(Guid categoryId, string searchName)
        {
            return Filter(categoryId, searchName).OrderByDescending(p => p.Price).ToList();
        }
    }
}
