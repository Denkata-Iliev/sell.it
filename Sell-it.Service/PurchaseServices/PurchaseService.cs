﻿using Microsoft.EntityFrameworkCore;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sell_it.Service.PurchaseServices
{
    public class PurchaseService : BaseService<Purchase>, IPurchaseService
    {
        private readonly DbSet<Purchase> dbSet;
        public PurchaseService(SellItContext context) : base(context)
        {
            dbSet = context.Set<Purchase>();
        }

        public new List<Purchase> GetAll()
        {
            return dbSet.Select(p => new Purchase
            {
                Id = p.Id,
                BuyerId = p.BuyerId,
                Buyer = new User
                {
                    Id = p.Buyer.Id,
                    UserName = p.Buyer.UserName
                },
                ProductId = p.ProductId,
                Product = new Product
                {
                    Name = p.Product.Name,
                    Description = p.Product.Description,
                    Price = p.Product.Price,
                    CategoryId = p.Product.CategoryId,
                    UserId = p.Product.UserId
                },
                Timestamp = p.Timestamp
            }).ToList();
        }
    }
}
