﻿using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.PurchaseServices
{
    public interface IPurchaseService : IBaseService<Purchase>
    {
    }
}