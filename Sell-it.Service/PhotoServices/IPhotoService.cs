﻿using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.PhotoServices
{
    public interface IPhotoService : IBaseService<Photo>
    {
        List<Photo> GetProductPhotos(Guid productId);

        void UpdateProductPhotos(Product product, ICollection<string> newImageUrls);
    }
}
