﻿using Microsoft.EntityFrameworkCore;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.PhotoServices
{
    public class PhotoService : BaseService<Photo>, IPhotoService
    {
        private readonly SellItContext context;
        private readonly DbSet<Photo> dbSet;

        public PhotoService(SellItContext context) : base(context)
        {
            dbSet = context.Set<Photo>();
        }

        public List<Photo> GetProductPhotos(Guid productId)
        {
            return dbSet.Where(p => p.ProductId.Equals(productId)).ToList();
        }

        public void UpdateProductPhotos(Product product, ICollection<string> newImageUrls)
        {
            if (product.Photos.Count == newImageUrls.Count)
            {
                for (int i = 0; i < product.Photos.Count; i++)
                {
                    UpdateCurrentPhoto(product, newImageUrls, i);
                }
            }
            else if (product.Photos.Count < newImageUrls.Count)
            {
                for (int i = 0; i < product.Photos.Count; i++)
                {
                    UpdateCurrentPhoto(product, newImageUrls, i);
                }

                for (int i = product.Photos.Count; i < newImageUrls.Count; i++)
                {
                    Photo photo = new Photo()
                    {
                        Id = new Guid(),
                        Path = newImageUrls.ElementAt(i),
                        ProductId = product.Id
                    };

                    product.Photos.Add(photo);
                }

            }
            else
            {
                for (int i = 0; i < newImageUrls.Count; i++)
                {
                    UpdateCurrentPhoto(product, newImageUrls, i);
                }

                while (newImageUrls.Count != product.Photos.Count)
                {
                    Photo currentPhoto = product.Photos.ElementAt(newImageUrls.Count);
                    product.Photos.Remove(currentPhoto);
                    Delete(currentPhoto);
                }
            }
        }

        private void UpdateCurrentPhoto(Product product, ICollection<string> newImageUrls, int index)
        {
            product.Photos.ElementAt(index).Path = newImageUrls.ElementAt(index);
            Update(product.Photos.ElementAt(index));
        }
    }
}
