﻿using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.CategoryServices
{
    public interface ICategoryService : IBaseService<Category>
    {
        Category GetProductCategory(Guid categoryId);
    }
}