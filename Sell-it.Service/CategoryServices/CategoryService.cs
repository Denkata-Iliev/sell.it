﻿using Microsoft.EntityFrameworkCore;
using Sell_it.Data;
using Sell_it.Data.Entities;
using Sell_it.Service.BaseServices;

namespace Sell_it.Service.CategoryServices
{
    public class CategoryService : BaseService<Category>, ICategoryService
    {
        private readonly SellItContext context;
        private readonly DbSet<Category> dbSet;
        public CategoryService(SellItContext context) : base(context)
        {
            this.context = context;
            dbSet = context.Set<Category>();
        }

        public Category GetProductCategory(Guid categoryId)
        {
            return dbSet.Find(categoryId);
        }

        public new void Update(Category categoryToUpdate)
        {
            var local = dbSet.Local.FirstOrDefault(c => c.Id.Equals(categoryToUpdate.Id));

            if (local != null)
            {
                context.Entry(local).State = EntityState.Detached;
            }

            context.Entry(categoryToUpdate).State = EntityState.Modified;

            context.SaveChanges();
        }

        public new void Delete(Category category)
        {
            Category miscCategory = context.Categories.FirstOrDefault(c => c.Name.Equals("Misc"));

            if (category.Id.Equals(miscCategory.Id))
            {
                return;
            }

            var productsDeletedCategory = context.Products.Where(p => p.Category.Id.Equals(category.Id));

            dbSet.Remove(category);

            foreach (var item in productsDeletedCategory)
            {
                item.Category = miscCategory;
            }

            context.Products.UpdateRange(productsDeletedCategory);

            context.SaveChanges();
        }
    }
}
