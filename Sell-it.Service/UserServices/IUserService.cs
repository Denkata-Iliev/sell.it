﻿using Sell_it.Data.Entities;

namespace Sell_it.Service.UserServices
{
    public interface IUserService
    {
        User Get(string userId);
    }
}