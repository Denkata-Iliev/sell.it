﻿using Sell_it.Data;
using Sell_it.Data.Entities;

namespace Sell_it.Service.UserServices
{
    public class UserService : IUserService
    {
        private readonly SellItContext context;

        public UserService(SellItContext context)
        {
            this.context = context;
        }

        public User Get(string userId)
        {
            return context.Users.Find(userId);
        }
    }
}
