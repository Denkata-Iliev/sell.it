﻿using Sell_it.Data.Entities;

namespace Sell_it.Service.BaseServices
{
    public interface IBaseService<T> where T : BaseEntity
    {
        List<T> GetAll();
        T Get(Guid id);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);

    }
}