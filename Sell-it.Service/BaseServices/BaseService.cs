﻿using Microsoft.EntityFrameworkCore;
using Sell_it.Data;
using Sell_it.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sell_it.Service.BaseServices
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        private readonly SellItContext context;
        private readonly DbSet<T> dbSet;

        public BaseService(SellItContext context)
        {
            this.context = context;
            dbSet = context.Set<T>();
        }

        public List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public T Get(Guid id)
        {
            return dbSet.Find(id);
        }

        public void Create(T entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            dbSet.Update(entity);
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            context.SaveChanges();
        }
    }
}
