﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sell_it.Data.Entities;

namespace Sell_it.Data
{
    public class SellItContext : IdentityDbContext<User>
    {
        public override DbSet<User> Users { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Purchase> Purchases { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public SellItContext(DbContextOptions<SellItContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Product>()
                   .HasOne(p => p.Category)
                   .WithMany(c => c.Products);

            builder.Entity<Product>()
                   .HasMany(p => p.Photos)
                   .WithOne(ph => ph.Product)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}
