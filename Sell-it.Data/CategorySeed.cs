﻿using Sell_it.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sell_it.Data
{
    public class CategorySeed
    {
        public static void SeedMiscCategory(SellItContext context)
        {
            if (context.Categories.Where(c => c.Name.Equals("Misc")).Any())
            {
                return;
            }

            context.Categories.Add(new Category
            {
                Id = new Guid(),
                Name = "Misc"
            });

            context.SaveChanges();
        }
    }
}
