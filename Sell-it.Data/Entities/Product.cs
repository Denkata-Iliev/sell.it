﻿namespace Sell_it.Data.Entities
{
    public class Product : BaseEntity
    {

        public string Name { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public string UserId { get; set; }
        public User Seller { get; set; }

        public ICollection<Photo> Photos { get; set; }

        public Product()
        {
            Photos = new List<Photo>();
        }
    }
}
