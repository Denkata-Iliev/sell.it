﻿namespace Sell_it.Data.Entities
{
    public class Purchase : BaseEntity
    {

        public Guid ProductId { get; set; }
        public Product Product { get; set; }

        public string BuyerId { get; set; }
        public User Buyer { get; set; }

        public DateTime Timestamp { get; set; }

        public Purchase()
        {
            Timestamp = DateTime.Now;
        }
    }
}
