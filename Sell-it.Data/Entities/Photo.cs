﻿namespace Sell_it.Data.Entities
{
    public class Photo : BaseEntity
    {
        public string Path { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }
    }
}
