﻿namespace Sell_it.Data.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
