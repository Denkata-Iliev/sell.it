﻿using Microsoft.AspNetCore.Identity;

namespace Sell_it.Data.Entities
{
    public class User : IdentityUser
    {
        public ICollection<Product> Products { get; set; }

        public User()
        {
            Products = new List<Product>();
        }
    }
}
