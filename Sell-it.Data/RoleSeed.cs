﻿using Microsoft.AspNetCore.Identity;
using Sell_it.Data.Entities;

namespace Sell_it.Data
{
    public class RoleSeed
    {
        public static void SeedRoles(SellItContext context)
        {
            if(context.Roles.Any())
            {
                return;
            }

            context.Roles.Add(new IdentityRole()
            {
                Name = Roles.User.ToString(),
                NormalizedName = Roles.User.ToString().ToUpper()
            });

            context.Roles.Add(new IdentityRole()
            {
                Name = Roles.Admin.ToString(),
                NormalizedName = Roles.Admin.ToString().ToUpper()
            });

            context.SaveChanges();
        }

        public static async Task SeedAdmin(UserManager<User> userManager)
        {
            string email = "admin@admin.com";
            var user = await userManager.FindByEmailAsync(email);
            if (user == null)
            {
                User admin = new User()
                {
                    UserName = email,
                    NormalizedUserName = email.ToUpper(),
                    Email = email,
                    NormalizedEmail = email.ToUpper()
                };

                await userManager.CreateAsync(admin, "Admin1@admin.com");
                await userManager.AddToRoleAsync(admin, Roles.Admin.ToString());
            }

            if (userManager.GetRolesAsync(user).Result.Contains(Roles.Admin.ToString()))
            {
                return;
            }

            await userManager.RemoveFromRoleAsync(user, Roles.User.ToString());
            await userManager.AddToRoleAsync(user, Roles.Admin.ToString());
        }
    }
}
