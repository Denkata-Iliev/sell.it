# Sell.it

## **What is Sell.it?**
Sell.it is a new platform where you can sell (shocking, I know) and buy revolutionary products from all over the world.

## **How do I use it?**
The first thing you'll see when you go to our website is a list of all the available products that you can currently buy (requires an account).

![App screenshot](assets/AllProducts.png)

## **Buying and selling**
*Both require an account!*

- ## Selling a product

If you want to sell a product of your own, you can easily do so with the "Create Product" button, which will redirect you to a page with a form that you have to fill in. 

![App Screenshot](assets/CreateProduct.png)

After that, you will be redirected to the home page where you will see your product along with everyone else's.

You can edit all of your products by clicking on the "Edit" button. The form is the same as the Create form.

- ## Buying a product
You can buy a product by clicking on the "Buy" button.

You will be redirected to a page where you will see all of the product's details. 

Once you click on the "Buy Product" button there, you will be redirected to the "My Purchases" page where you will notice that your purchase has been made. You can also see all of your other purchases in this page.

![App Screenshot](assets/BuyProduct.png)

- ## **My Products**
By clicking on the "My Products" button, you will be redirected to a page, where you can see all of your products.

![App Screenshot](assets/MyProducts.png)

## **Admin Panel**
If you are logged in as an Admin, you have access to the admin panel. It gives you access to the users' roles (User or Admin) and allows you to change them. It also gives you permission to create/edit/delete categories.